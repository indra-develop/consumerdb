package com.mapfre.mydream.consumer.util;

public class ConsumerConstantes {
	public static final String TIPO_CAMPO_S = "S";
	public static final String TIPO_CAMPO_L = "L";
	public static final String TIPO_CAMPO_C = "C";
	
	public static final String TIPO_DATO_ENTRADA = "E";
	public static final String TIPO_DATO_SALIDA = "S";
	
	public static final String TIPO_DATO_NO_VARIBLE = "N";
	
	public static final String COD_ERROR_CONSUMER_EJB = "EJB";
	public static final String DESC_ERROR_CONSUMER_CONEXION = "Error Conexión:";
	public static final String DESC_ERROR_CONSUMER_GENERICO = "Error Genérico:";
	public static final String DESC_ERROR_CONSUMER_SENTENCIA = "Error al ejecutar sentencia";
	
	public static final String ID_PRODUCTO = "ID_PRODUCTO";
	public static final String ID_FASE = "ID_FASE";
	public static final String LIST_INTERPRETE = "LIST_INTERPRETE";
	public static final String LIST_OPERACION = "LIST_OPERACION";
	public static final String LIST_CAMPOWS = "LIST_CAMPOWS";
	public static final String EXP_REG_NAMESPACE = "[,+|]";
	public static final String INTERPRETE_OUTPUT_TYPE = "S";
	public static final String INTERPRETE_INPUT_TYPE = "E";
	public static final String E12_CODE = "E12";
	public static final String E12_MSG = "Parametros de entrada incorrectos";
	public static final String E06_CODE = "E06";
	public static final String E06_MSG = "Error de acceso";
	
	public static final String LISTA_INTERPRETE_GRUPO = "listaInterpreteGrupo";
	public static final String LISTA_ITEMS_GRUPO = "listaItemsGrupo";
	
	private ConsumerConstantes(){
	}
}