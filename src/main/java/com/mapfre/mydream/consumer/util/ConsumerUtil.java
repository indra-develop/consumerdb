package com.mapfre.mydream.consumer.util;

public class ConsumerUtil {
	private static final String SOAP12_NS_URI = "http://www.w3.org/2003/05/soap-envelope";
	public static final String SOAP12_ENVELOPE_HEAD = 
	        "<?xml version='1.0' encoding='utf-8'?>" + 
	        "<soapenv:Envelope xmlns:soapenv=\"" + SOAP12_NS_URI + "\">" +
	        "<soapenv:Header />" + 
	        "<soapenv:Body>";
	public static final String SOAP12_ENVELOPE_TAIL = 
	        "</soapenv:Body>" + 
	        "</soapenv:Envelope>";
	
	private static final String SOAP11_NS_URI = "http://schemas.xmlsoap.org/soap/envelope/";
	public static final String SOAP11_ENVELOPE_HEAD = 
	        "<?xml version='1.0' encoding='utf-8'?>" + 
	        "<soapenv:Envelope xmlns:soapenv=\"" + SOAP11_NS_URI + "\">" +
	        "<soapenv:Header />" + 
	        "<soapenv:Body>";
	public static final String SOAP11_ENVELOPE_TAIL = 
		        "</soapenv:Body>" + 
		        "</soapenv:Envelope>";
	 
	public static final String SOAP11 = "1.1";
	public static final String SOAP12 = "1.2";
	
	private ConsumerUtil(){
	}
}