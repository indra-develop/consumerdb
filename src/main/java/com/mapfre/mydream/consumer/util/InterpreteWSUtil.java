package com.mapfre.mydream.consumer.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.mapfre.mydream.consumer.dto.InterpreteWsDTO;
import com.mapfre.mydream.consumer.util.ConsumerConstantes;
import com.mapfre.mydream.json.Item;

public class InterpreteWSUtil {
	private InterpreteWSUtil() {
	}

	public static Map<Integer, InterpreteWsDTO> filterInterpreteWSByType(Map<Integer, InterpreteWsDTO> listaInterprete, String tipo) {
		Map<Integer, InterpreteWsDTO> result = new HashMap<>();
		Iterator<Integer> iterListaInterprete = listaInterprete.keySet().iterator();
		while(iterListaInterprete.hasNext()){
			Integer indiceListaInter = iterListaInterprete.next();
			InterpreteWsDTO interpreteWS = listaInterprete.get(indiceListaInter);
			if (interpreteWS.getTipo().equals(tipo)) {
				result.put(interpreteWS.getIdCampoWs(), interpreteWS);
			}
		}
		return result;
	}
	
	public static List<Item> filterItemByType(List<InterpreteWsDTO> interprete, String isVariable) {
		List<Item> listresult = new ArrayList<>();
		for (InterpreteWsDTO dataInterprete : interprete) {
			if (dataInterprete.getIsVariable().equals(isVariable) && ConsumerConstantes.TIPO_DATO_ENTRADA.equals(dataInterprete.getTipo())) {
				Item item = new Item();
				item.setId(dataInterprete.getCampo());
				item.setValor(dataInterprete.getValorDefect());
				listresult.add(item);
			}
		}
		return listresult;
	}
}