package com.mapfre.mydream.consumer.service;

import java.util.List;
import java.util.Map;

import com.mapfre.mydream.consumer.dto.CampoServicioDto;
import com.mapfre.mydream.consumer.dto.CampoWsDTO;
import com.mapfre.mydream.consumer.dto.InterpreteWsDTO;
import com.mapfre.mydream.consumer.soap.ConfiguracionConsumer;
import com.mapfre.mydream.json.Item;
public interface ConsumerService {
	public Map<String, Object> obtenerDatosProducto(Integer idProducto);
	public Object[] obtenerInterpreteWS(int codProducto, String codFase);
	public Object[] obtenerInterpreteAtajo(String codCampo);
}