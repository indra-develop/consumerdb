package com.mapfre.mydream.consumer.service.impl;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLStreamHandler;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.xml.namespace.QName;
import javax.xml.soap.AttachmentPart;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.Name;
import javax.xml.soap.Node;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPConstants;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPFault;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;
import javax.xml.soap.Text;

import org.apache.log4j.Logger;

import com.mapfre.mydream.consumer.dao.ConsumerDao;
import com.mapfre.mydream.consumer.dao.impl.ConsumerDaoImpl;
import com.mapfre.mydream.consumer.dto.CampoServicioDto;
import com.mapfre.mydream.consumer.dto.CampoWsDTO;
import com.mapfre.mydream.consumer.dto.InterpreteWsDTO;
import com.mapfre.mydream.consumer.service.ConsumerService;
import com.mapfre.mydream.consumer.soap.ConfiguracionConsumer;
import com.mapfre.mydream.consumer.soap.ObjectConsume;
import com.mapfre.mydream.consumer.util.ConsumerConstantes;
import com.mapfre.mydream.consumer.util.ConsumerUtil;
import com.mapfre.mydream.consumer.util.InterpreteWSUtil;
import com.mapfre.mydream.exception.BusinessException;
import com.mapfre.mydream.exception.GenericException;
import com.mapfre.mydream.json.FileAttachment;
import com.mapfre.mydream.json.Item;
import com.mapfre.mydream.util.EncodeUtil;
import com.mapfre.mydream.util.GenericConstants;
import com.mapfre.mydream.util.SplitUtil;

public class ConsumeServiceImpl implements ConsumerService {
	private static final Logger LOGGER = Logger.getLogger(ConsumeServiceImpl.class.getCanonicalName());
	private static final String TIPO_CAMPO_LISTA = "L";
	private static final String MENSAJE_ERROR_SOAP = "Error SOAP:";
	private static final String MENSAJE_ERROR_GENERICO = "Error Genérico:";
	private static final String STRING_VACIO = "";
	private static final String STRING_CERO = "0";
	private static final Integer INTEGER_CERO = 0;
	private static final String ID_MENSAJE_ERROR = "[msg_error]";
	
	private Map<String, Map<String, Item>> listaItemSalida;
	private Map<Integer, Map<String, SOAPElement>> listaSoapElement;
	private String cadena;
	private Deque<ObjectConsume> pilaConsume;
	
	private ConsumerDao consumerDao;
	
	public ConsumeServiceImpl() {
		consumerDao = new ConsumerDaoImpl();
	}

	@Override
	public Map<String, Object> obtenerDatosProducto(Integer idProducto) {
		if(consumerDao == null){
			consumerDao = new ConsumerDaoImpl();
		}
		return consumerDao.obtenerDatosProducto(idProducto);
	}

	@Override
	public Object[] obtenerInterpreteWS(int codProducto, String codFase) {
		if(consumerDao == null){
			consumerDao = new ConsumerDaoImpl();
		}
		return consumerDao.obtenerInterpreteWS(codProducto, codFase);
	}

	@Override
	public Object[] obtenerInterpreteAtajo(String codCampo) {
		if(consumerDao == null){
			consumerDao = new ConsumerDaoImpl();
		}
		return consumerDao.obtenerInterpreteAtajo(codCampo);
	}
	
	private SOAPElement agregarChildElement(SOAPElement soapElement, CampoServicioDto campoServicio, String valorNodo) throws SOAPException {
		if(valorNodo == null){
			if(campoServicio.getEtiqueta() != null){
				soapElement = soapElement.addChildElement(campoServicio.getNomCampoWs(), campoServicio.getEtiqueta());
			}else{
				soapElement = soapElement.addChildElement(campoServicio.getNomCampoWs());
			}
		}else{
			if(campoServicio.getEtiqueta() != null){
				soapElement = soapElement.addChildElement(campoServicio.getNomCampoWs(), campoServicio.getEtiqueta()).addTextNode(valorNodo);
			}else{
				soapElement = soapElement.addChildElement(campoServicio.getNomCampoWs()).addTextNode(valorNodo);
			}
		}
		
		return soapElement;
	}
	
	private void generarListaItem(List<Item>listaItem, String indicePadre, int indicador){
		if(listaItem != null){
			for(int i = 0; i < listaItem.size(); i++){
				Item itemIn =  listaItem.get(i);
				String indice = indicePadre;
				
				if(indicador == 0){
					if(listaItemSalida.get(itemIn.getId()) == null){
						listaItemSalida.put(itemIn.getId(), new TreeMap<String, Item>());
					}
					listaItemSalida.get(itemIn.getId()).put(STRING_CERO, itemIn);
				}else{
					if(indicePadre.isEmpty()){
						indice = String.valueOf(i);
					}else{
						if(indicador%2 == 0){
							indice = indicePadre;
						}else if(indicador%2 == 1){
							indice = indicePadre + "." + i;
						}
					}
					if(indicador%2 == 0){
						if(listaItemSalida.get(itemIn.getId()) == null){
							listaItemSalida.put(itemIn.getId(), new TreeMap<String, Item>());
						}
						listaItemSalida.get(itemIn.getId()).put(indice, itemIn);
					}
				}
				
				if(itemIn.getItems() != null && !itemIn.getItems().isEmpty()){
					int newIndicador = indicador + 1;
					generarListaItem(itemIn.getItems(), indice, newIndicador);
				}
			}
		}
	}
	
	private void addAttachment(List<FileAttachment> listFile, SOAPMessage soapMessage) {
		for (FileAttachment file : listFile) {
			AttachmentPart attachment = soapMessage.createAttachmentPart();
			byte[] jpegData2 = EncodeUtil.base64ToByte(file.getFileData());
			InputStream is2 = new ByteArrayInputStream(jpegData2);
			FileDataSource fileDataSource2 = new FileDataSource(EncodeUtil.inputStreamToFile(is2));
			DataHandler dataHandler2 = new DataHandler(fileDataSource2);
			attachment.setDataHandler(dataHandler2);
			attachment.setContentId(file.getContentId());
			soapMessage.addAttachmentPart(attachment);
		}
	}
}