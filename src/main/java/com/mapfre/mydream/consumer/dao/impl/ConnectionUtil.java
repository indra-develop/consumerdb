package com.mapfre.mydream.consumer.dao.impl;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mapfre.mydream.exception.GenericException;

public class ConnectionUtil {
	private static final Logger	LOGGER = Logger.getLogger(ConnectionUtil.class);
	
	private ConnectionUtil(){
	}
	
	public static Connection getConnection() {
		Connection conn = null;
		
		Properties prop = new Properties();
		InputStream input = null;

		try {
			input = ConnectionUtil.class.getClassLoader().getResourceAsStream("datasourceconsumer.properties");
			prop.load(input);
			
			Context ctx = new InitialContext();
			DataSource ds = (DataSource) ctx.lookup(prop.getProperty("ejb.datasource"));
			conn = ds.getConnection();
		} catch (IOException | NamingException | SQLException e) {
			LOGGER.error("Error Connection:", e);
			throw new GenericException("E100","Error al obtener conexión");
		}
		return conn;
	}
}