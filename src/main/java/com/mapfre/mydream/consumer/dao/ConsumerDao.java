package com.mapfre.mydream.consumer.dao;

import java.util.Map;

public interface ConsumerDao {
	public Object[] obtenerInterpreteWS(int codProducto, String codFase);
	public Object[] obtenerInterpreteAtajo(String codCampo);
	public Map<String, Object> obtenerDatosProducto(Integer idProducto);
	
}
