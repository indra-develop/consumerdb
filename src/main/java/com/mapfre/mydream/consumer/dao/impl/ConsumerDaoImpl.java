package com.mapfre.mydream.consumer.dao.impl;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.mapfre.mydream.consumer.dao.ConsumerDao;
import com.mapfre.mydream.consumer.dto.CampoWsDTO;
import com.mapfre.mydream.consumer.dto.InterpreteWsDTO;
import com.mapfre.mydream.consumer.dto.ServicioConfigDTO;
import com.mapfre.mydream.consumer.util.ConsumerConstantes;
import com.mapfre.mydream.exception.GenericException;

public class ConsumerDaoImpl implements ConsumerDao {
	private static final Logger LOGGER = Logger.getLogger(ConsumerDaoImpl.class.getCanonicalName());
	
	@Override
	public Object[] obtenerInterpreteWS(int codProducto, String codFase) {
		Connection conn = null;
		CallableStatement cs = null;
		Object[] result = new Object[3];
		try {
			conn = ConnectionUtil.getConnection();
			cs = null;
			String call = "{call mydream_consumesoap.p_recupera_interprete_ws(?,?,?,?,?)}";
			cs = conn.prepareCall(call);
			cs.setInt(1, codProducto);
			cs.setString(2, codFase);
			cs.registerOutParameter(3, -10);// OracleTypes.CURSOR
			cs.registerOutParameter(4, -10);// OracleTypes.CURSOR
			cs.registerOutParameter(5, -10);// OracleTypes.CURSOR
			cs.executeUpdate();

			ResultSet rsO = (ResultSet) cs.getObject(3);
			ResultSet rsI = (ResultSet) cs.getObject(4);
			ResultSet rsC = (ResultSet) cs.getObject(5);

			result = obtenerListasWS(rsO, rsI, rsC);

		} catch (Exception e) {
			LOGGER.error(ConsumerConstantes.DESC_ERROR_CONSUMER_GENERICO, e);
			throw new GenericException(ConsumerConstantes.COD_ERROR_CONSUMER_EJB, ConsumerConstantes.DESC_ERROR_CONSUMER_SENTENCIA);

		} finally {
			if (cs != null) {
				try {
					cs.close();
				} catch (SQLException e) {
					LOGGER.error(ConsumerConstantes.DESC_ERROR_CONSUMER_CONEXION, e);
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					LOGGER.error(ConsumerConstantes.DESC_ERROR_CONSUMER_CONEXION, e);
				}
			}
		}
		return result;
	}

	@Override
	public Object[] obtenerInterpreteAtajo(String codCampo) {
		Connection conn = null;
		CallableStatement cs = null;
		Object[] result = new Object[3];
		try {
			conn = ConnectionUtil.getConnection();
			cs = null;

			String call = "{call mydream_consumesoap.p_recupera_interprete_atajo(?,?,?,?)}";
			cs = conn.prepareCall(call);
			cs.setString(1, codCampo);
			cs.registerOutParameter(2, -10);// OracleTypes.CURSOR
			cs.registerOutParameter(3, -10);// OracleTypes.CURSOR
			cs.registerOutParameter(4, -10);// OracleTypes.CURSOR

			cs.executeUpdate();
			ResultSet rsO = (ResultSet) cs.getObject(2);
			ResultSet rsI = (ResultSet) cs.getObject(3);
			ResultSet rsC = (ResultSet) cs.getObject(4);

			result = obtenerListasWS(rsO, rsI, rsC);
			LOGGER.debug("-->result: " + result);
		} catch (Exception e) {
			LOGGER.error(ConsumerConstantes.DESC_ERROR_CONSUMER_GENERICO, e);
			throw new GenericException(ConsumerConstantes.COD_ERROR_CONSUMER_EJB, ConsumerConstantes.DESC_ERROR_CONSUMER_SENTENCIA);
		} finally {
			if (cs != null) {
				try {
					cs.close();
				} catch (SQLException e) {
					LOGGER.error(ConsumerConstantes.DESC_ERROR_CONSUMER_CONEXION, e);
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					LOGGER.error(ConsumerConstantes.DESC_ERROR_CONSUMER_CONEXION, e);
				}
			}
		}
		return result;
	}
	
	private Object[] obtenerListasWS(ResultSet rsO, ResultSet rsI, ResultSet rsC)
			throws SQLException {
		Object[] result = new Object[3];

		List<ServicioConfigDTO> lO = new ArrayList<>();
		while (rsO.next()) {
			ServicioConfigDTO o = new ServicioConfigDTO();
			o.setIdOperacion(rsO.getInt("ID_OPERACION"));
			o.setUrl(rsO.getString("WSDL"));
			o.setOperacion(rsO.getString("OPERACION"));
			o.setOrden(rsO.getInt("ORDEN"));
			o.setNamespace(rsO.getString("NAMESPACE"));
			o.setEtiqueta(rsO.getString("ETIQUETA"));
			o.setVersionSOAP(rsO.getString("VERSION_SOAP"));
			o.setTimeOut(rsO.getInt("TIME_OUT"));
			lO.add(o);
		}
		result[0] = lO;

		Map<Integer, List<InterpreteWsDTO>> lIMap = new HashMap<>();
		while (rsI.next()) {
			InterpreteWsDTO i = new InterpreteWsDTO();
			i.setIdOperacion(rsI.getInt("ID_SERVICIO"));
			i.setCampo(rsI.getString("CAMPO"));
			i.setIdCampoWs(rsI.getInt("ID_CAMPOWS"));
			i.setTipo(rsI.getString("TIPO"));
			i.setValida(rsI.getString("VALIDA"));
			i.setIsVariable(rsI.getString("MCA_VAR"));
			i.setTipoValidacion(rsI.getString("TIP_VALIDACION"));
			i.setValorDefect(rsI.getString("VAL_DEFECTO"));
			i.setIsCompuesto(rsI.getString("TIP_OBJETO"));
			i.setNombreObject(rsI.getString("NOMBRE_OBJECT"));
			i.setGrupo(rsI.getInt("GRUPO"));

			if(lIMap.get(i.getIdOperacion()) == null){
				lIMap.put(i.getIdOperacion(), new ArrayList<InterpreteWsDTO>());
			}
			lIMap.get(i.getIdOperacion()).add(i);
		}
		result[1] = lIMap;

		Map<Integer, Map<Integer, CampoWsDTO>> lCMap = new HashMap<>();
		while (rsC.next()) {
			CampoWsDTO c = new CampoWsDTO();
			c.setIdCampoWs(rsC.getInt("ID_CAMPOWS"));
			c.setNomCampoWs(rsC.getString("NOM_CAMPOWS"));
			c.setIdPadreWs(rsC.getInt("ID_CAMPO_PADRE"));
			c.setEtiqueta(rsC.getString("ETIQUETA"));
			c.setIdOperacion(rsC.getInt("ID_OPERACION"));

			if(lCMap.get(c.getIdOperacion()) == null){
				lCMap.put(c.getIdOperacion(), new HashMap<Integer, CampoWsDTO>());
			}
			
			lCMap.get(c.getIdOperacion()).put(c.getIdCampoWs(), c);
		}
		result[2] = lCMap;

		return result;
	}

	@Override
	public Map<String, Object> obtenerDatosProducto(Integer idProducto) {
		Connection conn = null;
		PreparedStatement ps = null;
		Map<String, Object> result = new HashMap<>();
		try {
			conn = ConnectionUtil.getConnection();
			String select = "SELECT cod_cia AS CODIGOCOMPANIA, cod_ramo AS CODIGORAMO, cod_modalidad AS CODIGOMODALIDAD "
					+ "FROM prod_mydream_tronweb "
					+ "WHERE cod_producto = ? "
					+ "AND mca_inh = ? ";
			ps = conn.prepareStatement(select);

			ps.setInt(1, idProducto);
			ps.setString(2, "N");

			ResultSet rs = ps.executeQuery();

			if (rs.next()) {
				result.put("codCia", rs.getInt("CODIGOCOMPANIA"));
				result.put("codRamo", rs.getInt("CODIGORAMO"));
				result.put("codModalidad", rs.getInt("CODIGOMODALIDAD"));
			}
		} catch (Exception e) {
			LOGGER.error(ConsumerConstantes.DESC_ERROR_CONSUMER_GENERICO, e);
			throw new GenericException(ConsumerConstantes.COD_ERROR_CONSUMER_EJB, ConsumerConstantes.DESC_ERROR_CONSUMER_SENTENCIA);
		} finally {
			if (ps != null) {
				try {
					ps.close();
				} catch (SQLException e) {
					LOGGER.error(ConsumerConstantes.DESC_ERROR_CONSUMER_CONEXION, e);
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					LOGGER.error(ConsumerConstantes.DESC_ERROR_CONSUMER_CONEXION, e);
				}
			}
		}
		return result;
	}
}