package com.mapfre.mydream.consumer.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.mapfre.mydream.consumer.dto.CampoServicioDto;

public class CampoServicioDto implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//Campo CampoWS
	private Integer idOperacion;
	private Integer idCampoWs;
	private String nomCampoWs;
	private Integer idPadreWs;
	private String etiqueta;
	
	//Campo Interprete_Operacion
	private String campo;
	private String tipo;
	private String valida;
	private String valorDefect;
	private String isVariable;
	private String tipoValidacion;
	private String nombreObject;
	private String isCompuesto;
	private Integer grupo;
	
	//Nivel de Lista
	private Integer nivelLista;
	
	//Campos Hijos
	private List<CampoServicioDto> camposHijos;

	public Integer getIdOperacion() {
		return idOperacion;
	}

	public void setIdOperacion(Integer idOperacion) {
		this.idOperacion = idOperacion;
	}

	public Integer getIdCampoWs() {
		return idCampoWs;
	}

	public void setIdCampoWs(Integer idCampoWs) {
		this.idCampoWs = idCampoWs;
	}

	public String getNomCampoWs() {
		return nomCampoWs;
	}

	public void setNomCampoWs(String nomCampoWs) {
		this.nomCampoWs = nomCampoWs;
	}

	public Integer getIdPadreWs() {
		return idPadreWs;
	}

	public void setIdPadreWs(Integer idPadreWs) {
		this.idPadreWs = idPadreWs;
	}

	public String getEtiqueta() {
		return etiqueta;
	}

	public void setEtiqueta(String etiqueta) {
		this.etiqueta = etiqueta;
	}

	public String getCampo() {
		return campo;
	}

	public void setCampo(String campo) {
		this.campo = campo;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getValida() {
		return valida;
	}

	public void setValida(String valida) {
		this.valida = valida;
	}

	public String getValorDefect() {
		return valorDefect;
	}

	public void setValorDefect(String valorDefect) {
		this.valorDefect = valorDefect;
	}

	public String getIsVariable() {
		return isVariable;
	}

	public void setIsVariable(String isVariable) {
		this.isVariable = isVariable;
	}

	public String getTipoValidacion() {
		return tipoValidacion;
	}

	public void setTipoValidacion(String tipoValidacion) {
		this.tipoValidacion = tipoValidacion;
	}

	public String getNombreObject() {
		return nombreObject;
	}

	public void setNombreObject(String nombreObject) {
		this.nombreObject = nombreObject;
	}

	public String getIsCompuesto() {
		return isCompuesto;
	}

	public void setIsCompuesto(String isCompuesto) {
		this.isCompuesto = isCompuesto;
	}

	public Integer getGrupo() {
		return grupo;
	}

	public void setGrupo(Integer grupo) {
		this.grupo = grupo;
	}
	
	public List<CampoServicioDto> getCamposHijos() {
		if(camposHijos == null){
			camposHijos = new ArrayList<>();
		}
		return camposHijos;
	}

	public Integer getNivelLista() {
		return nivelLista;
	}

	public void setNivelLista(Integer nivelLista) {
		this.nivelLista = nivelLista;
	}

	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}
}