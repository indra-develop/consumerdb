package com.mapfre.mydream.consumer.dto;

import java.io.Serializable;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

public class CampoWsDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	private Integer idOperacion;
	private Integer idCampoWs;
	private String nomCampoWs;
	private Integer idPadreWs;
	private String etiqueta;

	public Integer getIdCampoWs() {
		return idCampoWs;
	}

	public void setIdCampoWs(Integer idCampoWs) {
		this.idCampoWs = idCampoWs;
	}

	public String getNomCampoWs() {
		return nomCampoWs;
	}

	public void setNomCampoWs(String nomCampoWs) {
		this.nomCampoWs = nomCampoWs;
	}

	public Integer getIdPadreWs() {
		return idPadreWs;
	}

	public void setIdPadreWs(Integer idPadreWs) {
		this.idPadreWs = idPadreWs;
	}

	public String getEtiqueta() {
		return etiqueta;
	}

	public void setEtiqueta(String etiqueta) {
		this.etiqueta = etiqueta;
	}

	public Integer getIdOperacion() {
		return idOperacion;
	}

	public void setIdOperacion(Integer idOperacion) {
		this.idOperacion = idOperacion;
	}

	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}
}