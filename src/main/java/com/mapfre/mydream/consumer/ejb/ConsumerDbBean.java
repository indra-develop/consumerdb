package com.mapfre.mydream.consumer.ejb;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.mapfre.mydream.consumer.dto.CampoServicioDto;
import com.mapfre.mydream.consumer.dto.CampoWsDTO;
import com.mapfre.mydream.consumer.dto.InterpreteWsDTO;
import com.mapfre.mydream.consumer.dto.ServicioConfigDTO;
import com.mapfre.mydream.consumer.service.ConsumerService;
import com.mapfre.mydream.consumer.service.impl.ConsumeServiceImpl;
import com.mapfre.mydream.consumer.soap.ConfiguracionConsumer;
import com.mapfre.mydream.consumer.util.ConsumerConstantes;
import com.mapfre.mydream.consumer.util.InterpreteWSUtil;
import com.mapfre.mydream.exception.GenericException;
import com.mapfre.mydream.json.FileAttachment;
import com.mapfre.mydream.json.Item;

public class ConsumerDbBean {
private static final Logger LOGGER = Logger.getLogger(ConsumerDbBean.class);
	private static final Integer INTEGER_CERO = 0;
	
	private ConsumerService consumerService;

	public ConsumerDbBean() {
		consumerService = new ConsumeServiceImpl();
	}
	
	public Map<String, Object> obtenerInterpreteAtajo(String codCampo) {
		if (consumerService == null) {
			consumerService = new ConsumeServiceImpl();
		}
		Map<String, Object> params = new HashMap<>();
		Object[] oArr = consumerService.obtenerInterpreteAtajo(codCampo);
		List<ServicioConfigDTO> listOperacion = (List<ServicioConfigDTO>) oArr[0];
		if (listOperacion == null || listOperacion.isEmpty()) {
			throw new GenericException("E10",
					"No existe operación asociada al código de campo "
							+ codCampo);
		}
		params.put(ConsumerConstantes.LIST_OPERACION, oArr[0]);
		params.put(ConsumerConstantes.LIST_INTERPRETE, oArr[1]);
		params.put(ConsumerConstantes.LIST_CAMPOWS, oArr[2]);
		return params;
	}

	public List<Item> obtenerDatosProducto(List<Item> items, Integer idProducto) {
		if (consumerService == null) {
			consumerService = new ConsumeServiceImpl();
		}
		Map<String, Object> map = consumerService
				.obtenerDatosProducto(idProducto);
		for (Map.Entry<String, Object> entry : map.entrySet()) {
			String key = entry.getKey();
			Object value = entry.getValue();
			Item item = new Item();
			item.setId(key);
			item.setValor(value.toString());
			items.add(item);
		}
		return items;
	}

	public Map<String, Object> obtenerInterpreteWS(int codProducto, String codFase) {
		if (consumerService == null) {
			consumerService = new ConsumeServiceImpl();
		}
		Map<String, Object> params = new HashMap<>();
		Object[] oArr = consumerService.obtenerInterpreteWS(codProducto,
				codFase);
		List<ServicioConfigDTO> listOperacion = (List<ServicioConfigDTO>) oArr[0];
		if (listOperacion == null || listOperacion.isEmpty()) {
			throw new GenericException("E10",
					"No existe operación asociada a la fase " + codFase);
		}
		params.put(ConsumerConstantes.LIST_OPERACION, oArr[0]);
		params.put(ConsumerConstantes.LIST_INTERPRETE, oArr[1]);
		params.put(ConsumerConstantes.LIST_CAMPOWS, oArr[2]);
		return params;
	}
	
	
}