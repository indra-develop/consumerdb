package com.mapfre.mydream.consumer.soap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ObjectConsume {
	private String clave;
	private String tipo;
	private String valor;
	private String padre;
	private Integer estado;
	private Map<String, Object> map;
	private List<Map<String,Object>> mapList;	

	public ObjectConsume() {
		this.map = new HashMap<>();
	}
	
	public ObjectConsume(String clave, String tipo, String valor, String padre) {
		this.map = new HashMap<>();
		this.mapList = new ArrayList<>();
		this.clave = clave;
		this.tipo = tipo;
		this.padre = padre;
		this.estado = 0;
		this.valor = valor;
	}
	
	public List<Map<String, Object>> getMapList() {
		return mapList;
	}

	public void setMapList(List<Map<String, Object>> mapList) {
		this.mapList = mapList;
	}

	public Integer getEstado() {
		return estado;
	}

	public void setEstado(Integer estado) {
		this.estado = estado;
	}

	public String getPadre() {
		return padre;
	}

	public void setPadre(String padre) {
		this.padre = padre;
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public Map<String, Object> getMap() {
		return map;
	}

	public void setMap(Map<String, Object> map) {
		this.map = map;
	}

	public String getClave() {
		return clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}

	@Override
	public String toString() {
		return "ObjectConsume [clave=" + clave + ", tipo=" + tipo + ", valor="
				+ valor + ", padre=" + padre + ", estado=" + estado + ", map="
				+ map + "]";
	}
}