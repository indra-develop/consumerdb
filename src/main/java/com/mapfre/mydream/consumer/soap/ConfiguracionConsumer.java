package com.mapfre.mydream.consumer.soap;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.mapfre.mydream.consumer.dto.CampoServicioDto;
import com.mapfre.mydream.consumer.dto.InterpreteWsDTO;
import com.mapfre.mydream.consumer.dto.ServicioConfigDTO;
import com.mapfre.mydream.json.FileAttachment;
import com.mapfre.mydream.json.Item;

public class ConfiguracionConsumer {
	private ServicioConfigDTO servicioConfig;
	private Map<Integer, Map<Integer, CampoServicioDto>> listaCampoServicio;
	private Map<Integer, Map<Integer, CampoServicioDto>> listaCampoServicioSalida;
	private List<Item> listaItems;
	private Map<Integer, InterpreteWsDTO> listaInterpreteOutput;
	private List<FileAttachment> listaFile;

	public ServicioConfigDTO getServicioConfig() {
		return servicioConfig;
	}

	public void setServicioConfig(ServicioConfigDTO servicioConfig) {
		this.servicioConfig = servicioConfig;
	}

	public Map<Integer, Map<Integer, CampoServicioDto>> getListaCampoServicio() {
		return listaCampoServicio;
	}

	public void setListaCampoServicio(
			Map<Integer, Map<Integer, CampoServicioDto>> listaCampoServicio) {
		this.listaCampoServicio = listaCampoServicio;
	}

	public Map<Integer, Map<Integer, CampoServicioDto>> getListaCampoServicioSalida() {
		return listaCampoServicioSalida;
	}

	public void setListaCampoServicioSalida(
			Map<Integer, Map<Integer, CampoServicioDto>> listaCampoServicioSalida) {
		this.listaCampoServicioSalida = listaCampoServicioSalida;
	}

	public List<Item> getListaItems() {
		return listaItems;
	}

	public void setListaItems(List<Item> listaItems) {
		this.listaItems = listaItems;
	}

	public Map<Integer, InterpreteWsDTO> getListaInterpreteOutput() {
		return listaInterpreteOutput;
	}

	public void setListaInterpreteOutput(
			Map<Integer, InterpreteWsDTO> listaInterpreteOutput) {
		this.listaInterpreteOutput = listaInterpreteOutput;
	}

	public List<FileAttachment> getListaFile() {
		return listaFile;
	}

	public void setListaFile(List<FileAttachment> listaFile) {
		this.listaFile = listaFile;
	}

	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}
}