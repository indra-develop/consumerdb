package com.mapfre.mydream.consumer.soap;

import java.io.Serializable;

import javax.xml.soap.SOAPElement;

public class ItemSoap implements Serializable {
	private static final long serialVersionUID = 1L;

	private Integer id;
	private transient SOAPElement soapElement;

	public ItemSoap() {
		super();
	}

	public ItemSoap(Integer id, SOAPElement soapElement) {
		super();
		this.id = id;
		this.soapElement = soapElement;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public SOAPElement getSoapElement() {
		return soapElement;
	}

	public void setSoapElement(SOAPElement soapElement) {
		this.soapElement = soapElement;
	}

	@Override
	public String toString() {
		return "ItemSoap [id=" + id + ", soapElement=" + getSoapElement() + "]";
	}
}