package com.mapfre.mydream.consumer.soap;

import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.mapfre.mydream.json.Item;

public class GrupoOperacion {
	private Integer codGrupo;
	private Map<Integer, Map<Integer, Item>> data;

	public GrupoOperacion() {
		super();
		this.data = new TreeMap<>();
	}

	public Integer getCodGrupo() {
		return codGrupo;
	}

	public void setCodGrupo(Integer codGrupo) {
		this.codGrupo = codGrupo;
	}

	public Map<Integer, Map<Integer, Item>> getData() {
		return data;
	}

	public void setData(Map<Integer, Map<Integer, Item>> data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}
}